/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define SPI4_CSN_SBC_Pin GPIO_PIN_3
#define SPI4_CSN_SBC_GPIO_Port GPIOE
#define TSG2_T3_Pin GPIO_PIN_4
#define TSG2_T3_GPIO_Port GPIOE
#define I2C2_WP_Pin GPIO_PIN_2
#define I2C2_WP_GPIO_Port GPIOF
#define MCO_Pin GPIO_PIN_0
#define MCO_GPIO_Port GPIOH
#define RMII_MDC_Pin GPIO_PIN_1
#define RMII_MDC_GPIO_Port GPIOC
#define RMII_MDIO_Pin GPIO_PIN_2
#define RMII_MDIO_GPIO_Port GPIOA
#define RMII_RXD0_Pin GPIO_PIN_4
#define RMII_RXD0_GPIO_Port GPIOC
#define RMII_RXD1_Pin GPIO_PIN_5
#define RMII_RXD1_GPIO_Port GPIOC
#define DO_Analog_Hall_Pin GPIO_PIN_15
#define DO_Analog_Hall_GPIO_Port GPIOF
#define SBC_Int_Pin GPIO_PIN_12
#define SBC_Int_GPIO_Port GPIOE
#define CAN_FD2_Stb_Pin GPIO_PIN_11
#define CAN_FD2_Stb_GPIO_Port GPIOB
#define RMII_TXD0_Pin GPIO_PIN_12
#define RMII_TXD0_GPIO_Port GPIOB
#define RMII_TXD1_Pin GPIO_PIN_13
#define RMII_TXD1_GPIO_Port GPIOB
#define DO_K2_2_Pin GPIO_PIN_15
#define DO_K2_2_GPIO_Port GPIOB
#define DO_K2_1_Pin GPIO_PIN_8
#define DO_K2_1_GPIO_Port GPIOD
#define DO_K1_2_Pin GPIO_PIN_10
#define DO_K1_2_GPIO_Port GPIOD
#define DO_K1_1_Pin GPIO_PIN_11
#define DO_K1_1_GPIO_Port GPIOD
#define Mot2_DO_DRV_EN_Pin GPIO_PIN_2
#define Mot2_DO_DRV_EN_GPIO_Port GPIOG
#define Mot1_DO_DRV_EN_Pin GPIO_PIN_3
#define Mot1_DO_DRV_EN_GPIO_Port GPIOG
#define Mot2_DI_Fault_Pin GPIO_PIN_4
#define Mot2_DI_Fault_GPIO_Port GPIOG
#define Mot1_DI_Fault_Pin GPIO_PIN_5
#define Mot1_DI_Fault_GPIO_Port GPIOG
#define Mot2_DI_WDFault_Pin GPIO_PIN_6
#define Mot2_DI_WDFault_GPIO_Port GPIOG
#define Mot1_DI_WDFault_Pin GPIO_PIN_7
#define Mot1_DI_WDFault_GPIO_Port GPIOG
#define MAX3227_readyTX_Pin GPIO_PIN_8
#define MAX3227_readyTX_GPIO_Port GPIOG
#define SPI4_MOT1_DO_CSS_Pin GPIO_PIN_8
#define SPI4_MOT1_DO_CSS_GPIO_Port GPIOC
#define SPI4_MOT2_DO_CSS_Pin GPIO_PIN_9
#define SPI4_MOT2_DO_CSS_GPIO_Port GPIOC
#define DO_NTC_SUPPLY_Pin GPIO_PIN_8
#define DO_NTC_SUPPLY_GPIO_Port GPIOA
#define DO_MOT1_H_BRIDGE_Pin GPIO_PIN_10
#define DO_MOT1_H_BRIDGE_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define SPI3_RS_ARZ1_Pin GPIO_PIN_0
#define SPI3_RS_ARZ1_GPIO_Port GPIOD
#define SPI3_CS_AR1_Pin GPIO_PIN_1
#define SPI3_CS_AR1_GPIO_Port GPIOD
#define SPI3_CS_AR10_Pin GPIO_PIN_2
#define SPI3_CS_AR10_GPIO_Port GPIOD
#define SPI3_RS_AR10_Pin GPIO_PIN_3
#define SPI3_RS_AR10_GPIO_Port GPIOD
#define SPI3_SHDN_AR10_Pin GPIO_PIN_4
#define SPI3_SHDN_AR10_GPIO_Port GPIOD
#define SPI3_SHDN_AR1_Pin GPIO_PIN_7
#define SPI3_SHDN_AR1_GPIO_Port GPIOD
#define MAX3227_activeRX_Pin GPIO_PIN_10
#define MAX3227_activeRX_GPIO_Port GPIOG
#define RMII_TX_EN_Pin GPIO_PIN_11
#define RMII_TX_EN_GPIO_Port GPIOG
#define MAX3227_FORCEON_Pin GPIO_PIN_12
#define MAX3227_FORCEON_GPIO_Port GPIOG
#define MAX3227_FORCEOFF_Pin GPIO_PIN_15
#define MAX3227_FORCEOFF_GPIO_Port GPIOG
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
#define TSG1_T1_Pin GPIO_PIN_4
#define TSG1_T1_GPIO_Port GPIOB
#define TSG1_T2_Pin GPIO_PIN_8
#define TSG1_T2_GPIO_Port GPIOB
#define TSG1_T3_Pin GPIO_PIN_9
#define TSG1_T3_GPIO_Port GPIOB
#define TSG2_T1_Pin GPIO_PIN_0
#define TSG2_T1_GPIO_Port GPIOE
#define TSG2_T2_Pin GPIO_PIN_1
#define TSG2_T2_GPIO_Port GPIOE

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
